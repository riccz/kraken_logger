import pytest


@pytest.mark.asyncio
async def test_ping(kraken_ws):
    await kraken_ws.ping()


@pytest.mark.asyncio
async def test_one_trade_channel(kraken_ws):
    channel = await kraken_ws.subscribe(name='trade', pair='XBT/USD')
    assert channel.pair == 'XBT/USD'
    assert channel.subscription.name == 'trade'

    for _ in range(3):
        msg = await channel.receive()
        assert isinstance(msg, list)
        assert len(msg) == 6
        (price, volume, time, side, order_type, misc) = msg
        assert isinstance(price, str)
        assert isinstance(volume, str)
        assert isinstance(time, str)
        assert side in ('b', 's')
        assert order_type in ('m', 'l')
        assert isinstance(misc, str)


@pytest.mark.asyncio
async def test_one_depth_channel(kraken_ws):
    channel = await kraken_ws.subscribe(name='book', pair='XBT/USD', depth=25)
    assert channel.pair == 'XBT/USD'
    assert channel.subscription.name == 'book'
    assert channel.subscription.depth == 25
    assert channel.name == 'book-25'

    msg = await channel.receive()
    assert isinstance(msg, dict)
    assert sorted(msg.keys()) == sorted(['as', 'bs'])


@pytest.mark.asyncio
async def test_many_trade_channel(kraken_ws):
    xbtusd = await kraken_ws.subscribe(name='trade', pair='XBT/USD')
    xbteur = await kraken_ws.subscribe(name='trade', pair='XBT/EUR')
    ethusd = await kraken_ws.subscribe(name='trade', pair='ETH/USD')

    assert xbtusd.pair == 'XBT/USD'
    assert xbtusd.subscription.name == 'trade'
    assert xbteur.pair == 'XBT/EUR'
    assert xbteur.subscription.name == 'trade'
    assert ethusd.pair == 'ETH/USD'
    assert ethusd.subscription.name == 'trade'

    assert len(set([xbtusd.id, xbteur.id, ethusd.id])) == 3

    for channel in [xbtusd, xbteur, ethusd]:
        for _ in range(3):
            msg = await channel.receive()
            assert isinstance(msg, list)
            assert len(msg) == 6
            (price, volume, time, side, order_type, misc) = msg
            assert isinstance(price, str)
            assert isinstance(volume, str)
            assert isinstance(time, str)
            assert side in ('b', 's')
            assert order_type in ('m', 'l')
            assert isinstance(misc, str)
