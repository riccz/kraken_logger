import pytest

from kraken_logger.restapi import KrakenREST
from kraken_logger.wsapi import KrakenWebSocket

# The event loop is function-scoped
@pytest.fixture
async def kraken_rest(request):
    if request.config.getoption("--kraken-api"):
        async with KrakenREST() as api:
            api._private_rate_limiter.flush()
            yield api
    else:
        pytest.skip("give the `--kraken-api` option to run this test")


# The event loop is function-scoped
@pytest.fixture
async def kraken_ws(request, kraken_rest):
    if request.config.getoption("--kraken-api"):
        async with KrakenWebSocket(rest_api=kraken_rest) as api:
            api._rate_limiter.flush()
            yield api
    else:
        pytest.skip("give the `--kraken-api` option to run this test")


def pytest_addoption(parser):
    parser.addoption(
        "--kraken-api", action="store_true", default=False,
        help="run tests that make queries to the Kraken REST/WS APIs"
    )
