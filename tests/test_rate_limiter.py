import asyncio
import math
import time
from dataclasses import dataclass
from itertools import accumulate

import pytest
from hypothesis import assume, example, given
from hypothesis.strategies import (composite, floats, integers, lists, none,
                                   one_of, tuples)

from kraken_logger.rate_limiter import (AsyncRateLimiter, RateLimiter,
                                        RateLimitError, TokenBucket)

from .common import assert_execution_time


@dataclass
class Timer:
    value: float

    def get_value(self):
        return self.value

    def step(self, amount: float):
        next = self.value + amount
        assert next >= self.value
        assert math.isfinite(next)
        self.value = next


@given(fill_rate=floats(min_value=0, exclude_min=True,
                        allow_nan=False, allow_infinity=False),
       capacity=integers(min_value=1),
       costs=lists(integers(min_value=0)))
def test_bucket_throughput_int_timer(fill_rate, capacity, costs):
    timer = Timer(0)
    bucket = TokenBucket(fill_rate, capacity, initial_capacity=0,
                         time_source=timer.get_value)

    tot_consumed = 0
    for cost in costs:
        timer.step(1)
        if bucket.try_consume(cost):
            tot_consumed += cost
    tot_time = timer.get_value()

    if tot_time == 0:
        throughput = math.inf if tot_time != 0 else 0
    else:
        throughput = tot_consumed / tot_time
    assert throughput <= fill_rate


@given(fill_rate=floats(min_value=0, exclude_min=True,
                        allow_nan=False, allow_infinity=False),
       capacity=integers(min_value=1),
       costs_times=lists(tuples(
           integers(min_value=0),
           floats(min_value=0, allow_nan=False, allow_infinity=False))),
       start_time=floats(min_value=0, allow_nan=False, allow_infinity=False))
def test_bucket_throughput_any_timer(fill_rate, capacity,
                                     costs_times, start_time):
    timer = Timer(start_time)
    bucket = TokenBucket(fill_rate, capacity, initial_capacity=0,
                         time_source=timer.get_value)

    assume(math.isfinite(start_time + sum(t for _, t in costs_times)))

    tot_consumed = 0
    for cost, time in costs_times:
        timer.step(time)
        if bucket.try_consume(cost):
            tot_consumed += cost
    tot_time = timer.get_value() - start_time

    if tot_time == 0:
        throughput = math.inf if tot_time != 0 else 0
    else:
        throughput = tot_consumed / tot_time

    assert throughput <= fill_rate


def test_as_decorator():

    @RateLimiter(rate=1, block=True)
    def func():
        pass

    with assert_execution_time(4, 1):
        for _ in range(5):
            func()


def test_as_object():
    rl = RateLimiter(rate=1, block=True)

    def func():
        pass

    rl.add('func', func)
    with assert_execution_time(4, 1):
        for _ in range(5):
            rl.func()


def test_access():
    rl = RateLimiter(rate=1, block=True)
    def foo(): return 'FOO'
    def bar(): return 'BAR'

    rl.add('foo', foo)
    rl.add('bar', bar)

    assert rl.get('foo') is rl.foo
    assert rl.get('bar') is rl.bar

    assert rl.foo() == 'FOO'
    assert rl.bar() == 'BAR'


def test_multiple_funcs():
    rl = RateLimiter(rate=5, block=True)
    def foo(): return 'FOO'
    def bar(): return 'BAR'
    rl.add('foo', foo)
    rl.add('bar', bar)

    with assert_execution_time(3.8, 1):
        for _ in range(10):
            rl.foo()
            rl.bar()


def test_nonblocking():
    rl = RateLimiter(rate=1, block=False)
    def func(): pass
    rl.add('func', func)

    rl.func()
    with pytest.raises(RateLimitError):
        rl.func()

    time.sleep(1)
    rl.func()


def test_methods():
    class Example:
        def __init__(self):
            self._rl = RateLimiter(rate=2)

        def _get_rl(self):
            return self._rl

        @RateLimiter.add_method(_get_rl)
        def foo(self, x):
            return x**2

        @RateLimiter.add_method(_get_rl)
        def bar(self, y):
            return f'Hello {y}!'

    ex = Example()
    assert ex.foo(4) == 16
    assert ex.bar('world') == 'Hello world!'

    time.sleep(1/2)
    with assert_execution_time(2.5, 1):
        ex.foo(1)
        ex.bar('a')
        ex.bar('b')
        ex.bar('c')
        ex.foo(2)
        ex.foo(3)


def test_fixed_cost():
    rl = RateLimiter(rate=1, capacity=2)
    def func(): pass
    rl.add('func', func, cost=2)

    with assert_execution_time(2, 1):
        rl.func()
        rl.func()


def test_variable_cost():
    rl = RateLimiter(rate=1, capacity=10)
    def func(what): pass

    def cost(what):
        if what == 'heavy':
            return 10
        else:
            return 1
    rl.add('func', func, cost=cost)

    with assert_execution_time(1, 1):
        rl.func('heavy')
        rl.func('light')


def test_method_with_variable_cost():
    class Example:
        def __init__(self):
            self._rl = RateLimiter(rate=2, capacity=2)

        def _get_rl(self):
            return self._rl

        def _foo_cost(self, x):
            if x % 2 == 0:
                return 2
            else:
                return 1

        @RateLimiter.add_method(_get_rl, cost=_foo_cost)
        def foo(self, x):
            return x**2

    ex = Example()
    with assert_execution_time(2.5, 1):
        assert ex.foo(2) == 4
        assert ex.foo(3) == 9
        assert ex.foo(4) == 16
        assert ex.foo(6) == 36


def test_over_capacity():
    rl = RateLimiter(rate=100, capacity=1, block=False)
    def func(): pass
    rl.add('func', func, cost=2)

    with pytest.raises(RateLimitError):
        rl.func()


@pytest.mark.asyncio
async def test_async_method_with_variable_cost():
    class Example:
        def __init__(self):
            self._rl = AsyncRateLimiter(rate=2, capacity=2)

        def _get_rl(self):
            return self._rl

        def _foo_cost(self, x):
            if x % 2 == 0:
                return 2
            else:
                return 1

        @AsyncRateLimiter.add_method(_get_rl, cost=_foo_cost)
        async def foo(self, x):
            return x**2

    ex = Example()
    with assert_execution_time(2.5, 1):
        assert await ex.foo(2) == 4
        assert await ex.foo(3) == 9
        assert await ex.foo(4) == 16
        assert await ex.foo(6) == 36


@pytest.mark.asyncio
async def test_async_parallel_calls():
    rl = AsyncRateLimiter(rate=10, capacity=1)
    async def foo(): pass
    async def bar(): pass
    rl.add('foo', foo, cost=1)
    rl.add('bar', bar, cost=1)

    coros = [rl.foo() for _ in range(5)] + [rl.bar() for _ in range(5)]
    with assert_execution_time(0.9, 1):
        await asyncio.gather(*coros)

@pytest.mark.asyncio
async def test_async_parallel_calls_different_costs():
    rl = AsyncRateLimiter(rate=10, capacity=10, initial_capacity=0)
    async def foo(): pass
    async def bar(): pass
    rl.add('foo', foo, cost=1)
    rl.add('bar', bar, cost=10)

    coros = [rl.foo() for _ in range(5)] + [rl.bar() for _ in range(2)]
    with assert_execution_time(2.5, 1):
        await asyncio.gather(*coros)
