import asyncio

import pytest

from kraken_logger.common import async_retry

from .common import assert_execution_time


class SucceedAfter:
    def __init__(self, n_times: int):
        self.failures_left = n_times
        self.call_count = 0

    async def call(self):
        self.call_count += 1
        if self.failures_left > 0:
            self.failures_left -= 1
            raise RuntimeError("I failed!")
        else:
            return 'OK'


@pytest.mark.asyncio
async def test_async_retry_infinite():
    func = SucceedAfter(3)
    assert await async_retry(func.call) == 'OK'
    assert func.call_count == 4


@pytest.mark.asyncio
async def test_async_retry_limited_ok():
    foo = SucceedAfter(2)
    assert await async_retry(foo.call, max_tries=3) == 'OK'
    assert foo.call_count == 3


@pytest.mark.asyncio
async def test_async_retry_limited_fail():
    foo = SucceedAfter(2)
    with pytest.raises(RuntimeError) as exc:
        await async_retry(foo.call, max_tries=2)
    assert exc.value.args == ("I failed!",)
    assert foo.call_count == 2


@pytest.mark.asyncio
async def test_async_retry_with_wait():
    foo = SucceedAfter(3)
    with assert_execution_time(0.3, 2):
        assert await async_retry(foo.call, retry_wait=0.1) == 'OK'
    assert foo.call_count == 4


@pytest.mark.asyncio
async def test_async_retry_nowait_on_success():
    foo = SucceedAfter(0)
    with assert_execution_time(0, 0):
        assert await async_retry(foo.call, retry_wait=60) == 'OK'
    assert foo.call_count == 1


@pytest.mark.asyncio
async def test_async_retry_passthrough():
    class MyException(RuntimeError):
        pass

    async def raise_my_exception():
        raise MyException()

    raise_runtime_error = SucceedAfter(3).call

    assert await async_retry(raise_runtime_error, passthrough=(MyException,)) == 'OK'
    with pytest.raises(MyException):
        await async_retry(raise_my_exception, passthrough=(MyException,))
