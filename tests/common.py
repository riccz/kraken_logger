import time
from contextlib import contextmanager


@contextmanager
def assert_execution_time(expected, decimals):
    start = time.monotonic()
    yield
    elapsed = time.monotonic() - start
    assert round(elapsed, decimals) == expected


async def collect_async(async_gen, max_count=None):
    xs = []
    i = 0
    async for x in async_gen:
        if max_count is None or i < max_count:
            xs.append(x)
            i += 1
        else:
            break
    return xs
