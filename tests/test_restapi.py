import itertools
import locale
from datetime import datetime, timezone

import pytest

from kraken_logger.common import Trade


@pytest.mark.asyncio
async def test_remote_time(kraken_rest):
    response = await kraken_rest.query_public('Time')
    unixtime = response['result']['unixtime']
    rfc1123 = response['result']['rfc1123']

    locale.setlocale(locale.LC_TIME, 'en_US.UTF8')
    parsed_unix = datetime.fromtimestamp(unixtime, tz=timezone.utc)
    parsed_rfc = datetime.strptime(rfc1123, '%a, %d %b %y %H:%M:%S %z')
    assert parsed_rfc == parsed_unix


@pytest.mark.asyncio
async def test_trades(kraken_rest):
    (trades, last) = await kraken_rest.trades('XXBTZEUR')
    for t in trades:
        assert isinstance(t, Trade)
        assert t.pair == 'XXBTZEUR'


@pytest.mark.asyncio
async def test_trade_batch_iter(kraken_rest):
    cnt = 5000
    all_trades = []
    lasts = []
    async for trades, last in kraken_rest.trade_batch_iter('XXBTZEUR', since=0):
        assert len(trades) > 0
        for t in trades:
            assert isinstance(t, Trade)
            assert t.pair == 'XXBTZEUR'
        all_trades.append(trades)
        lasts.append(lasts)
        cnt -= len(trades)
        if cnt <= 0:
            break

    for x, y in itertools.combinations(all_trades, r=2):
        assert x != y


@pytest.mark.asyncio
async def test_trade_iter(kraken_rest):
    trade_iter = kraken_rest.trade_iter('XXBTZEUR', since=0)
    cnt = 5000
    async for trade in trade_iter:
        assert isinstance(trade, Trade)
        assert trade.pair == 'XXBTZEUR'
        cnt -= 1
        if cnt == 0:
            break
