import asyncio
import functools
import logging
import logging.config
from datetime import timedelta
from typing import List

import aiohttp
import asyncpg
import click
import yaml

from .common import Trade, async_retry
from .db import TradeSink, fetch_pair
from .restapi import KrakenREST
from .wsapi import KrakenWebSocket


logger = logging.getLogger(__name__)


async def log_ws_trades(pair_names: List[str],
                        sink: TradeSink,
                        kraken_ws: KrakenWebSocket,
                        max_tries: int = 0, retry_wait: float = 0):
    logger.info("Start WS trade logging")

    db_pool = sink.db_pool
    async with db_pool.acquire() as conn:
        pairs = [await fetch_pair(p, conn) for p in pair_names]

    async def log_one_channel(pair):
        chan = await kraken_ws.subscribe(name='trade', pair=pair['wsname'])
        while True:
            data = await chan.receive_batch()
            await sink.push(Trade.from_raw(chan.pair, d) for d in data)

    async def log_forever():
        await kraken_ws.reconnect()
        log_tasks = [asyncio.create_task(log_one_channel(p)) for p in pairs]
        await asyncio.gather(*log_tasks)

    await async_retry(log_forever, max_tries=max_tries, retry_wait=retry_wait,
                      passthrough=(KeyboardInterrupt, asyncio.CancelledError))
    logger.info("Stop WS trade logging")


async def validate_old_trades(pair_names: List[str],
                              sink: TradeSink,
                              kraken_rest: KrakenREST,
                              max_tries: int = 0, retry_wait: float = 0,
                              validate_interval: timedelta = timedelta(hours=1)):
    logger.info("Start old trade validation")

    db_pool = sink.db_pool

    async def validate_one_pair(pair_name):
        async with db_pool.acquire() as conn:
            pair = await fetch_pair(pair_name, conn)

        logger.info("Start validation of %r since %d",
                    pair['name'], pair['last'])
        async for (trades, last) in kraken_rest.trade_batch_iter(pair['name'], pair['last']):
            await sink.validate(trades, {pair['name']: last})
        logger.info("Validation of %r completed", pair['name'])

    async def validate_forever():
        while True:
            tasks = [asyncio.create_task(validate_one_pair(p))
                     for p in pair_names]
            await asyncio.gather(*tasks)
            logger.info("All validation tasks completed")
            await asyncio.sleep(validate_interval.total_seconds())

    await async_retry(validate_forever, max_tries=max_tries, retry_wait=retry_wait,
                      passthrough=(KeyboardInterrupt, asyncio.CancelledError))
    logger.info("Stop old trade validation")


def coro(async_func):
    @functools.wraps(async_func)
    def wrapper(*args, **kwargs):
        return asyncio.run(async_func(*args, **kwargs))
    return wrapper


@click.command()
@click.option('-c', '--config', 'config_file',
              default='config.yml', type=click.File('r'),
              help='Configuration file.')
@click.option('--ws-trades/--no-ws-trades', default=False,
              help='Log websocket trades.')
@click.option('--validate/--no-validate', default=False,
              help='Validate old trades.')
@coro
async def main(config_file, ws_trades, validate) -> int:
    config = yaml.safe_load(config_file.read())
    config_file.close()

    logging.config.dictConfig(config['logging'])

    db_config = config['db_connection'].copy()
    # psycopg2 vs asyncpg argument name
    db_config['database'] = db_config.get('dbname')
    del db_config['dbname']

    pair_names = config['include_pairs']

    async with asyncpg.create_pool(**db_config, min_size=1, max_size=10) as db_pool, \
            aiohttp.ClientSession() as http_session, \
            KrakenREST() as kraken_rest, \
            KrakenWebSocket(http_session=http_session, rest_api=kraken_rest) as kraken_ws:
        sink = TradeSink(db_pool)
        tasks = []
        if ws_trades:
            tasks.append(asyncio.create_task(log_ws_trades(pair_names,
                                                           sink,
                                                           kraken_ws,
                                                           retry_wait=3)))
        if validate:
            tasks.append(asyncio.create_task(validate_old_trades(pair_names,
                                                                 sink,
                                                                 kraken_rest,
                                                                 retry_wait=3)))
        try:
            await asyncio.gather(*tasks)
        except click.Abort:
            for task in tasks:
                task.cancel()
            await asyncio.sleep(10)
            return 1
    return 0
