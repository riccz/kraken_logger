import asyncio
import logging
from dataclasses import dataclass
from typing import Dict, Iterable, Optional

import asyncpg

from .common import Trade


logger = logging.getLogger(__name__)


@dataclass(eq=False)
class Pair:
    id: int
    name: str
    base: str
    quote: str
    altname: Optional[str] = None
    wsname: Optional[str] = None
    last: int = 0

    @staticmethod
    async def create(db_conn: asyncpg.Connection,
                     name: str, base: str, quote: str,
                     altname: Optional[str] = None,
                     wsname: Optional[str] = None,
                     last: int = 0) -> 'Pair':
        pair_id = await db_conn.fetchval(
            '''INSERT INTO pairs (name, base, quote, altname, wsname, last)
               VALUES ($1, $2, $3, $4, $5, $6)
               RETURNING id''', name, base, quote, altname, wsname, last)
        return Pair(
            id=pair_id,
            name=name,
            base=base,
            quote=quote,
            altname=altname,
            wsname=wsname,
            last=last
        )

    @staticmethod
    async def fetch(name: str, db_conn: asyncpg.Connection) -> 'Pair':
        pairs = await db_conn.fetch('SELECT * FROM pairs WHERE wsname = $1 '
                                    'OR name = $1 OR altname = $1', name)
        assert len(pairs) == 1
        pair = pairs[0]
        return Pair(
            id=pair['id'],
            name=pair['name'],
            base=pair['base'],
            quote=pair['quote'],
            altname=pair['altname'],
            wsname=pair['wsname'],
            last=pair['last']
        )

    def __str__(self):
        return self.name

    def __eq__(self, other):
        if not isinstance(other, Pair):
            return NotImplemented
        else:
            return self.id == other.id

    def __hash__(self):
        return hash(self.id)

    def _warn_changed_attrs(self, row, row_is_after=True):
        for attr in ('name', 'base', 'quote', 'altname', 'wsname'):
            if row_is_after:
                before = getattr(self, attr)
                after = row[attr]
            else:
                before = row[attr]
                after = getattr(self, attr)

            if before != after:
                logger.warning('Pair field %r should be immutable. '
                               'Changed %r -> %r',
                               attr, before, after)

        if row_is_after:
            last_before = self.last
            last_after = row['last']
        else:
            last_before = row['last']
            last_after = self.last

        if last_after < last_before:
            logger.warning('Pair field \'last\' should not decrease. '
                           'Changed %d -> %d', last_before, last_after)

    async def update(self, db_conn: asyncpg.Connection):
        pairs = await db_conn.fetch('SELECT * FROM pairs WHERE id = $1', self.id)
        assert len(pairs) == 1
        pair = pairs[0]

        self._warn_changed_attrs(pair)

        for attr in ('name', 'base', 'quote', 'altname', 'wsname', 'last'):
            setattr(self, attr, pair[attr])

    async def push(self, db_conn: asyncpg.Connection):
        old_pairs = await db_conn.fetch(
            '''WITH old as (SELECT * FROM pairs WHERE id = $7)
               UPDATE pairs SET name = $1, base = $2, quote = $3, altname = $4,
                                wsname = $5, last = $6
               FROM old
               WHERE id = $7
               RETURNING old.*''',
            self.name, self.base, self.quote, self.altname, self.wsname,
            self.last, self.id)
        assert len(old_pairs) == 1
        old_pair = old_pairs[0]
        self._warn_changed_attrs(old_pair, row_is_after=False)


async def fetch_pair(name: str, db_conn: asyncpg.Connection):
    pair = await db_conn.fetch(
        """\
        SELECT * FROM pairs WHERE wsname = $1 OR name = $1 OR altname = $1
        """, name)
    assert len(pair) == 1
    return pair[0]


class TradeSink:
    def __init__(self, db_pool: asyncpg.pool.Pool):
        self.db_pool = db_pool
        self._validate_lock = asyncio.Lock()

    async def push(self, trades: Iterable[Trade]):
        async with self.db_pool.acquire() as conn:
            async with conn.transaction():
                cnt = 0
                for t in trades:
                    await self._push_one(t, conn, data_source='pushed')
                    cnt += 1
        logger.debug("TradeSink committed %d trades", cnt)

    async def _push_one(self, trade: Trade, conn: asyncpg.Connection,
                        data_source: str, validated: bool = False):
        new_trade_id = await conn.fetchval(
            """\
            INSERT INTO trades (pair, price, volume, time, side, order_type,
                                misc, validated, data_source)
            VALUES ((SELECT id FROM pairs WHERE wsname = $1
                                                OR name = $1
                                                OR altname = $1),
                    $2, $3, $4, $5, $6, $7, $8, $9)
            RETURNING id
            """,
            trade.pair, trade.price, trade.volume, trade.time, trade.side,
            trade.order_type, trade.misc, validated, data_source
        )
        assert isinstance(new_trade_id, int)
        logger.debug("TradeSink%s inserted %r with id=%d",
                     (' [validated]' if validated else ''),
                     trade, new_trade_id)

    async def _validate_one(self, trade: Trade, conn: asyncpg.Connection):
        result = await conn.fetch(
            """\
            UPDATE trades SET validated = true
            WHERE id = (SELECT id FROM trades
                        WHERE pair = (
                            SELECT id FROM pairs WHERE wsname = $1
                                                 OR name = $1
                                                 OR altname = $1
                        )
                        AND price = $2
                        AND volume = $3
                        AND time = $4
                        AND side = $5
                        AND order_type = $6
                        AND (misc = $7
                             OR misc IS NULL AND $7 IS NULL)
                        AND NOT validated
                        LIMIT 1
            ) RETURNING id
            """,
            trade.pair, trade.price, trade.volume, trade.time, trade.side,
            trade.order_type, trade.misc
        )
        if len(result) > 0:
            ((validated_id,),) = result
            logger.debug("Validated existing trade %d (%r)",
                         validated_id, trade)
        else:
            await self._push_one(trade, conn, validated=True, data_source='validated_insert')

    async def validate(self, trades: Iterable[Trade], lasts: Dict[str, int]):
        async with self._validate_lock, self.db_pool.acquire() as conn:
            async with conn.transaction():
                cnt = 0
                for t in trades:
                    await self._validate_one(t, conn)
                    cnt += 1
                for pair_name, last in lasts.items():
                    (pair_id, upd_last) = await conn.fetchrow(
                        """UPDATE pairs SET last = $2
                           WHERE id = (SELECT id FROM pairs
                                       WHERE name = $1
                                       OR altname = $1
                                       OR wsname = $1
                                      )
                           RETURNING id, last
                        """, pair_name, last)
                    logger.debug("Updated pair %r id=%d: last=%d",
                                 pair_name, pair_id, upd_last)
            logger.debug("TradeSink validated %d trades", cnt)
