import asyncio
import logging
from collections.abc import Awaitable
from dataclasses import dataclass
from datetime import datetime, timezone
from decimal import Decimal
from typing import Callable, Optional, Tuple

logger = logging.getLogger(__name__)


@dataclass
class Trade:
    pair: str
    price: Decimal
    volume: Decimal
    time: datetime
    side: str
    order_type: str
    misc: Optional[str] = None

    @staticmethod
    def from_raw(pair: str, data: list):
        # pair_pieces = tuple(pair.split('/'))
        # if len(pair_pieces) != 2 or any(len(pp) < 1 for pp in pair_pieces):
        #     raise ValueError(f"Pair string {pair!r} is "
        #                      "not in format `XXX/YYY`")

        (price, volume, time, side, order_type, misc) = data

        if isinstance(price, float):
            logger.warning("Building a `Trade` with a `float` price. Data: %r",
                           data)
        price = Decimal(price)

        if isinstance(volume, float):
            logger.warning("Building a `Trade` with a `float` volume. Data: %r",
                           data)
        volume = Decimal(volume)

        time = datetime.fromtimestamp(float(time), tz=timezone.utc)

        side = side.lower()
        if side not in ('b', 's'):
            raise ValueError(f"Side is not `b` or `s`: {side!r}")

        order_type = order_type.lower()
        if order_type not in ('m', 'l'):
            raise ValueError("Order type is not `m` or `l`"
                             f": {order_type!r}")

        if misc == '':
            misc = None

        return Trade(pair, price, volume, time, side, order_type, misc)


async def async_retry(func: Callable[..., Awaitable], *args,
                      max_tries: int = 0, retry_wait: float = 0,
                      passthrough: Tuple[type, ...] = ()):
    try_count = 0
    while True:
        try_count += 1
        try:
            return await func(*args)
        except Exception as exc:
            if not isinstance(exc, passthrough) and (max_tries < 1 or
                                                     try_count < max_tries):
                logger.error("Call to %s failed. "
                             "Retry after %ss (%d/%d tries so far)",
                             func.__qualname__,
                             retry_wait, try_count, max_tries,
                             exc_info=exc)
                await asyncio.sleep(retry_wait)
            else:
                raise exc


def iter_except(func, exception, first=None):
    try:
        if first is not None:
            yield first()
        while True:
            yield func()
    except exception:
        pass
