import asyncio
import functools
import logging
import math
import time
from collections.abc import Awaitable
from typing import Any, Callable, Dict, Optional, Union


logger = logging.getLogger(__name__)


class TokenBucket:
    def __init__(self,
                 fill_rate: float,
                 capacity: int,
                 initial_capacity: Optional[int] = None,
                 time_source: Optional[Callable[[], float]] = None):
        assert fill_rate > 0.0
        assert math.isfinite(fill_rate)
        assert capacity >= 1
        if initial_capacity is None:
            initial_capacity = capacity
        assert initial_capacity >= 0
        assert initial_capacity <= capacity
        if time_source is None:
            time_source = time.monotonic

        self.fill_rate = fill_rate
        self.capacity = capacity
        self.time_source = time_source

        self._tokens = initial_capacity

        # Always call time_source through _now
        self._last_time = -math.inf  # Must be <= than any time_source()
        self._last_time = self._now()

    def try_consume(self, token_count=1) -> bool:
        if token_count > self.capacity:
            logger.warning("Trying to consume %(toks)d tokens "
                           "from a TokenBucket with "
                           "capacity %(cap)d < %(toks)d",
                           {'toks': token_count, 'cap': self.capacity})

        self._refill()
        if self._tokens >= token_count:
            self._tokens -= token_count
            logger.debug("Consumed %d tokens. Left with %d",
                         token_count, self._tokens)
            return True
        else:
            logger.debug("Cannot consume %d tokens. Only %d available",
                         token_count, self._tokens)
            return False

    def min_wait(self, token_count=1) -> float:
        missing = max(0, token_count - self._tokens)
        elapsed = self._now() - self._last_time
        return max(0.0, missing / self.fill_rate - elapsed)

    def _now(self):
        now = self.time_source()
        if not math.isfinite(now):
            raise ValueError("Time source of TokenBucket gave "
                             f"a non-finite time: {now!r}")
        elif now < self._last_time:
            raise ValueError("Time source of TokenBucket "
                             f"is running backward: now={now!r} "
                             f"< last_time={self._last_time!r}")
        else:
            return now

    def _refill(self):
        delta = self._now() - self._last_time
        tot_fill = delta * self.fill_rate
        logger.debug("Refilling TokenBucket after delta=%e; tot_fill=%e",
                     delta, tot_fill)
        if tot_fill >= self.capacity - self._tokens:
            self._tokens = self.capacity
            round_delta = delta
        else:
            new_tokens = math.floor(tot_fill)
            assert new_tokens >= 0
            self._tokens += new_tokens
            assert self._tokens <= self.capacity
            round_delta = new_tokens / self.fill_rate

        logger.debug("Refilled TokenBucket to %d, round_delta=%e",
                     self._tokens, round_delta)
        assert round_delta >= 0
        self._last_time += round_delta

    def flush(self):
        self._last_time = self._now()
        self._tokens = 0


class RateLimitError(RuntimeError):
    def __init__(self, func, min_wait):
        super().__init__(f"Called {func.__qualname__} too soon."
                         f" Needed to wait at least {min_wait}s more")
        self.func = func
        self.min_wait = min_wait


class _RateLimiterBase:
    def __init__(self, rate: float, capacity: int = 1, initial_capacity: int = None):
        self._bucket = TokenBucket(fill_rate=rate,
                                   capacity=capacity,
                                   initial_capacity=initial_capacity)
        self._funcs: Dict[str, Callable] = {}

    def _wrap(self, func: Callable, cost: Union[int, Callable[..., int]] = 1):
        raise NotImplementedError()

    def add(self, name: str, func: Callable, cost: Union[int, Callable[..., int]] = 1):
        self._funcs[name] = self._wrap(func, cost)

    def remove(self, name):
        del self._funcs[name]

    def get(self, name, default=None):
        return self._funcs.get(name, default)

    def __getattr__(self, name):
        return self._funcs[name]

    def __call__(self, func: Callable, cost: Union[int, Callable[..., int]] = 1):
        self.add('decorated', func, cost)
        return self.decorated

    @staticmethod
    def add_method(rate_limiter_getter: Callable[..., '_RateLimiterBase'],
                   cost: Union[int, Callable[..., int]] = 1):
        def wrapper(method):
            @functools.wraps(method)
            def wrapped(*args, **kwargs):
                self = args[0]
                rate_limiter = rate_limiter_getter(self)
                rl_method = rate_limiter.get(method.__name__)
                if rl_method is None:
                    rate_limiter.add(method.__name__, method, cost)
                    rl_method = rate_limiter.get(method.__name__)
                return rl_method(*args, **kwargs)
            return wrapped
        return wrapper

    def flush(self):
        self._bucket.flush()


class RateLimiter(_RateLimiterBase):
    def __init__(self,
                 rate: float,
                 capacity: int = 1,
                 initial_capacity: int = None,
                 block: bool = True):
        super().__init__(rate, capacity, initial_capacity)
        self.block = block

    def _wrap(self, func: Callable, cost: Union[int, Callable[..., int]] = 1):
        @functools.wraps(func)
        def wrapped(*args, **kwargs):
            if callable(cost):
                c = cost(*args, **kwargs)
            else:
                c = cost
            min_wait = self._bucket.min_wait(c)
            if min_wait > 0.0:
                if self.block:
                    time.sleep(min_wait)
                else:
                    raise RateLimitError(wrapped, min_wait)
            r = self._bucket.try_consume(c)
            assert r, "Should never happen: either wait or raise"
            return func(*args, **kwargs)
        return wrapped


class AsyncRateLimiter(_RateLimiterBase):
    def _wrap(self, func: Callable[..., Awaitable],
              cost: Union[int, Callable[..., int]] = 1):
        @functools.wraps(func)
        async def wrapped(*args, **kwargs):
            if callable(cost):
                c = cost(*args, **kwargs)
            else:
                c = cost

            while not self._bucket.try_consume(c):
                min_wait = self._bucket.min_wait(c)
                await asyncio.sleep(min_wait)

            return await func(*args, **kwargs)
        return wrapped
