import asyncio
import logging
from collections import deque
from datetime import datetime, timezone
from threading import Lock
from typing import Any, AsyncIterator, Deque, Dict, List, Optional, Tuple

from krakenex import API

from .common import Trade
from .rate_limiter import AsyncRateLimiter


logger = logging.getLogger(__name__)


class KrakenRESTError(RuntimeError):
    def __init__(self, call_args, result):
        super().__init__(
            f"KrakenRESTError: call_args={call_args!r}, result={result!r}")
        self.call_args = call_args
        self.result = result


class LLKrakenREST:
    """Asynchronous adapter of :class:`krakenex.API`.

    This class exposes the same interface of :class:`krakenex.API` as
    asynchronous methods.

    The call rate is limited according to `the API docs`_.

    .. _the API docs: https://www.kraken.com/features/api
    """

    def __init__(self, *args, user_level='starter', executor=None, **kwargs):
        self._api = API(*args, **kwargs)

        self._lock = Lock()
        self._executor = executor
        self._loop = asyncio.get_running_loop()

        if user_level == 'starter':
            private_rate = 1/3
            private_capacity = 15
        elif user_level == 'intermediate':
            private_rate = 1/2
            private_capacity = 20
        elif user_level == 'pro':
            private_rate = 1
            private_capacity = 20
        else:
            raise ValueError(f"User level {user_level!r} is unknown")

        # Limits are enforced by API key (except `AddOrder`, `CancelOrder`). See
        # <https://support.kraken.com/hc/en-us/articles/206548367-What-are-the-REST-API-rate-limits->
        self._private_rate_limiter = AsyncRateLimiter(rate=0.95*private_rate,
                                                      capacity=private_capacity)

        # TODO: Query different pairs simultaneously (same max freq: 1
        # call/sec).
        #
        # Limits are enforced by IP address (and currency pair for `Trades` and
        # `OHLC`)
        self._public_rate_limiter = AsyncRateLimiter(rate=0.95*0.5)

    def _get_public_rate_limiter(self):
        return self._public_rate_limiter

    def _get_private_rate_limiter(self):
        return self._private_rate_limiter

    def _private_query_cost(self, method, *args, **kwargs):
        if method in ('Ledgers', 'TradesHistory', 'ClosedOrders',
                      'OpenOrders', 'QueryOrders', 'QueryTrades',
                      'OpenPositions', 'QueryLedgers'):
            return 2
        elif method in ('AddOrder', 'CancelOrder'):
            # TODO: Per (account, pair) unspecified rate limit, based on the
            # order permanence in the book.
            return 0
        else:
            return 1

    async def _make_async(self, func, *args, **kwargs):
        def blocking_func():
            with self._lock:
                return func(*args, **kwargs)
        return await self._loop.run_in_executor(self._executor, blocking_func)

    async def __aenter__(self):
        return self

    async def __aexit__(self, exc_type, exc, tb):
        await self.close()

    async def json_options(self, **kwargs):
        return await self._make_async(self._api.json_options, **kwargs)

    async def close(self):
        return await self._make_async(self._api.close)

    async def load_key(self, path):
        return await self._make_async(self._api.load_key, path)

    @AsyncRateLimiter.add_method(_get_public_rate_limiter)
    async def query_public(self, *args, **kwargs):
        logger.debug("Send Kraken public query: %r, %r", args, kwargs)
        response = await self._make_async(self._api.query_public, *args, **kwargs)
        self._raise_on_error(response, args, kwargs,
                             self._public_rate_limiter)
        return response

    @AsyncRateLimiter.add_method(_get_private_rate_limiter, _private_query_cost)
    async def query_private(self, *args, **kwargs):
        logger.debug("Send Kraken private query: %r, %r", args, kwargs)
        response = await self._make_async(self._api.query_private, *args, **kwargs)
        self._raise_on_error(response, args, kwargs,
                             self._private_rate_limiter)
        return response

    def _raise_on_error(self, response, args, kwargs, rate_limiter):
        if sorted(response.keys()) not in(['error', 'result'], ['error']):
            raise KrakenRESTError({'args': args, 'kwargs': kwargs}, response)
        if response['error'] == ['EAPI:Rate limit exceeded']:
            logger.warning("Rate limit exceeded: flush the token bucket")
            rate_limiter.flush()
        if response['error'] != []:
            raise KrakenRESTError({'args': args, 'kwargs': kwargs}, response)


class KrakenREST(LLKrakenREST):
    async def time(self) -> datetime:
        response = await self.query_public('Time')
        unixtime = response['result']['unixtime']
        return datetime.fromtimestamp(unixtime, tz=timezone.utc)

    async def assets(self, assets: List[str] = []):
        raise NotImplementedError()
        # info, aclass only have 1 value
        # csv_assets = ','.join(assets)
        # req_data = {}
        # if csv_assets != '':
        #     req_data['asset'] = csv_assets
        # response = await self.query_public('Assets', data=req_data)

    async def asset_pairs(self, info=None, pair=None):
        raise NotImplementedError()

    async def ticker(self, pair):
        raise NotImplementedError()

    async def ohlc(self, pair, interval=None, since=None):
        raise NotImplementedError()

    async def depth(self, pair, count=None):
        raise NotImplementedError()

    async def trades(self, pair: str, since: Optional[int] = None) -> Tuple[List[Trade], int]:
        req_data: Dict[str, Any] = {'pair': pair}
        if since is not None:
            req_data['since'] = since
        response = await self.query_public('Trades', data=req_data)
        assert sorted(response['result'].keys()) == sorted(['last', pair])
        raw_trades = response['result'][pair]
        trades = [Trade.from_raw(pair, t) for t in raw_trades]
        last = int(response['result']['last'])
        logger.debug('Fetched %d %r trades since %d up to %d',
                     len(trades), pair, since, last)
        return (trades, last)

    async def trade_batch_iter(self, pair: str, since: Optional[int] = None) \
            -> AsyncIterator[Tuple[List[Trade], int]]:
        logger.debug('Iterate over batches of %r trades since %d', pair, since)
        while True:
            (trades, last) = await self.trades(pair, since)
            if trades:
                yield (trades, last)
                since = last
            else:
                break

    def trade_iter(self, pair: str, since: Optional[int] = None) -> 'KrakenTradeIter':
        return KrakenTradeIter(self, pair, since)

    async def spread(self, pair, since=None):
        raise NotImplementedError()


class KrakenTradeIter:
    def __init__(self, rest_api: KrakenREST, pair: str, since: Optional[int]):
        self.rest_api = rest_api
        self.pair = pair
        self.since = since

        self._last = since
        self._pending_trades: Deque[Trade] = deque()

    @property
    def last(self):
        if not self._pending_trades:
            return self._last
        else:
            raise RuntimeError(f'There are still {len(self._pending_trades)}'
                               ' pending trades')

    @property
    def pending_count(self):
        return len(self._pending_trades)

    def __aiter__(self) -> 'KrakenTradeIter':
        return self

    async def __anext__(self) -> Trade:
        if not self._pending_trades:
            (trades, last) = await self.rest_api.trades(self.pair, self._last)
            self._pending_trades.extend(trades)
            self._last = last

        if self._pending_trades:
            return self._pending_trades.popleft()
        else:
            raise StopAsyncIteration()
