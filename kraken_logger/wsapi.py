import asyncio
import logging
import math
import random
import time
from collections import deque
from dataclasses import dataclass
from typing import (Any, AsyncGenerator, Deque, Dict, Hashable, List, Optional,
                    Set, Union)

import aiohttp

from .rate_limiter import AsyncRateLimiter
from .restapi import KrakenREST


logger = logging.getLogger(__name__)


class KrakenWSError(RuntimeError):
    pass


class TokenFactory:
    def __init__(self, rest_api: KrakenREST, renew_at: float = 2/3):
        self.rest_api = rest_api
        self.renew_at = renew_at

        self._ws_token = None
        self._ws_token_expires = 0
        self._ws_token_time = -math.inf

    async def get_token(self) -> str:
        age = time.monotonic() - self._ws_token_time
        renew = self.renew_at * self._ws_token_expires
        if age >= renew:
            await self.refresh_token()
        assert self._ws_token is not None
        return self._ws_token

    async def refresh_token(self):
        result = await self.rest_api.query_private('GetWebSocketsToken')
        self._ws_token = result['result']['token']
        self._ws_token_expires = result['result']['expires']
        self._ws_token_time = time.monotonic()


class LLKrakenWS:
    def __init__(self, http_session: aiohttp.ClientSession):
        self.http_session = http_session
        self._wsconn = None
        self._wsconn_lock = asyncio.Lock()  # aiohttp raises on concurrent receive

        self._pending_reqids: Set[int] = set()
        self._public_responses: Dict[int, Dict] = {}

        self._last_private_response: Optional[Dict] = None
        self._privreqs_lock = asyncio.Lock()

        self._published_queue: Deque[Dict] = deque()

        # Rate limits are not documented. This is guessed
        self._rate_limiter = AsyncRateLimiter(rate=1, capacity=10)

    async def __aenter__(self):
        await self._get_wsconn()
        return self

    async def __aexit__(self, exc_type, exc, tb):
        await self.close()

    async def close(self):
        if self._wsconn is not None and not self._wsconn.closed:
            logger.debug("Close LLKrakenWS connection")
            await self._wsconn.close()

    async def reconnect(self):
        self._pending_reqids.clear()
        self._public_responses.clear()
        self._last_private_response = None
        self._published_queue.clear()
        await self._get_wsconn(reconnect=True)

    async def _get_wsconn(self, reconnect=False):

        async def connect():
            self._wsconn = await self.http_session.ws_connect(
                url='wss://ws.kraken.com',
                receive_timeout=10  # Heartbeat/msg approx every 1s
            )

        if self._wsconn is None:
            logger.debug("Connect LLKrakenWS")
            await connect()
        elif self._wsconn.closed and reconnect:
            logger.debug("Reconnect LLKrakenWS")
            await connect()

        return self._wsconn

    def _get_rate_limiter(self):
        return self._rate_limiter

    def generate_reqid(self):
        while True:
            reqid = random.randint(0, 2**31-1)
            if reqid not in self._pending_reqids:
                return reqid

    async def _receive(self):
        async with self._wsconn_lock:
            wsconn = await self._get_wsconn()
            data = await wsconn.receive_json()
            if 'errorMessage' in data:
                raise KrakenWSError(f"Error message: {data!r}")

            if isinstance(data, dict):
                if data.get('reqid') in self._pending_reqids:
                    logger.debug("LLKrakenWS received response for reqid=%d: %r",
                                 data['reqid'], data)
                    self._public_responses[data['reqid']] = data
                    return
                elif data.get('event') in ('addOrderStatus', 'cancelOrderStatus'):
                    logger.debug("LLKrakenWS received response for private request: %r",
                                 data)
                    assert self._last_private_response is None
                    self._last_private_response = data
                    return
            logger.debug("LLKrakenWS received published message: %r", data)
            self._published_queue.append(data)

    async def _receive_public_response(self, reqid: int):
        while reqid not in self._public_responses:
            await self._receive()
        response = self._public_responses[reqid]
        del self._public_responses[reqid]
        return response

    @AsyncRateLimiter.add_method(_get_rate_limiter)
    async def send_public_request(self, data: dict):
        if 'reqid' in data:
            reqid = data['reqid']
            assert reqid not in self._pending_reqids
        else:
            reqid = self.generate_reqid()
            data['reqid'] = reqid

        self._pending_reqids.add(reqid)
        async with self._wsconn_lock:
            wsconn = await self._get_wsconn()
            logger.debug("Send public WS request with data=%r", data)
            await wsconn.send_json(data=data)

        response = await self._receive_public_response(reqid)
        self._pending_reqids.remove(reqid)
        logger.debug("Received response for reqid=%d; response=%r",
                     reqid, response)
        return response

    @AsyncRateLimiter.add_method(_get_rate_limiter)
    async def _send_privreq_rl(self, data: dict):
        async with self._wsconn_lock:
            wsconn = await self._get_wsconn()
            logger.debug("Send private WS request with data=%r", data)
            await wsconn.send_json(data=data)

        response = await self._receive_private_response()
        logger.debug("Received private WS response: %r", response)
        return response

    async def send_private_request(self, data: dict):
        async with self._privreqs_lock:
            return await self._send_privreq_rl(data=data)

    async def _receive_private_response(self):
        while self._last_private_response is None:
            await self._receive()
        response = self._last_private_response
        self._last_private_response = None
        return response

    async def receive_published(self):
        while not self._published_queue:
            await self._receive()
        return self._published_queue.popleft()


@dataclass
class Subscription:
    depth: Optional[int]
    interval: Optional[int]
    token: Optional[str]
    name: str

    def as_dict(self) -> Dict[str, Union[str, int]]:
        data: Dict[str, Union[str, int]] = {'name': self.name}
        if self.depth is not None:
            data['depth'] = self.depth
        if self.interval is not None:
            data['interval'] = self.interval
        if self.token is not None:
            data['token'] = self.token
        return data


class Channel:
    def __init__(self, api, id, name, pair, subscription):
        self.api = api
        self.id = id
        self.name = name
        self.pair = pair
        self.subscription = subscription
        self.msg_queue = deque()
        self.unsubscribed = False

    def __repr__(self):
        return (f"{type(self)}(api={self.api!r}, id={self.id!r}, name={self.name!r},"
                f" pair={self.pair!r}, subscription={self.subscription!r})"
                f"[{'unsubscribed' if self.unsubscribed else 'subscribed'}]")

    async def unsubscribe(self):
        self._check_unsubscribed_flag()

        if self.id is not None:
            data = {
                'event': 'unsubscribe',
                'channelID': self.id
            }
        else:
            data = {
                'event': 'unsubscribe',
                'subscription': self.subscription.as_dict()
            }
            if self.pair is not None:
                data['pair'] = self.pair

        response = await self.api.send_public_request(data=data)
        assert response['event'] == 'subscriptionStatus'
        assert response['channelName'] == self.name
        assert response['status'] == 'unsubscribed'
        self.unsubscribed = True
        del self.api.channels[channel_key(self)]

    def _check_unsubscribed_flag(self):
        if self.unsubscribed:
            raise KrakenWSError("Trying to use the unsubscribed "
                                f"channel {self!r}")

    async def receive(self):
        self._check_unsubscribed_flag()

        while not self.msg_queue:
            logger.debug("Channel %r has no queued messages", self)
            await self.api._process_published()

        msg = self.msg_queue.popleft()
        logger.debug("Channel %r has received msg %r", self, msg)
        return msg

    def _try_enqueue(self, msg: Union[Dict, List]) -> bool:
        raise NotImplementedError()

    def receive_nowait(self):
        self._check_unsubscribed_flag()

        if self.msg_queue:
            return self.msg_queue.popleft()
        else:
            return None

    async def receive_batch(self):
        msgs = [await self.receive()]
        msg = self.receive_nowait()
        while msg is not None:
            msgs.append(msg)
            msg = self.receive_nowait()
        return msgs


class PublicChannel(Channel):
    def _try_enqueue(self, msg):
        if not isinstance(msg, list) or len(msg) != 4:
            return False
        (chan_id, payload, chan_name, pair) = msg
        if chan_id != self.id or chan_name != self.name or pair != self.pair:
            return False

        if isinstance(payload, list):
            self.msg_queue.extend(payload)
        else:
            self.msg_queue.append(payload)
        return True


class PrivateChannel(Channel):
    def __init__(self, api, name, subscription):
        super().__init__(api, None, name, None, subscription)

    def _try_enqueue(self, msg):
        if not isinstance(msg, list) or len(msg) != 2:
            return False
        (payload, chan_name) = msg
        if chan_name != self.name:
            return False

        if isinstance(payload, list):
            self.msg_queue.extend(payload)
        else:
            self.msg_queue.append(payload)
        return True


class KrakenWebSocket(LLKrakenWS):
    def __init__(self, http_session: Optional[aiohttp.ClientSession] = None,
                 rest_api: Optional[KrakenREST] = None,
                 renew_at: float = 2/3):
        if http_session is None:
            self._close_session = True
            http_session = aiohttp.ClientSession()
        else:
            self._close_session = False
        super().__init__(http_session=http_session)

        if rest_api is None:
            rest_api = KrakenREST()
        self.token_factory = TokenFactory(rest_api=rest_api, renew_at=renew_at)

        self.channels: Dict[Hashable, Channel] = {}

    async def close(self):
        for chan in self.channels.values():
            chan.unsubscribed = True
        self.channels.clear()

        await super().close()

        if self._close_session:
            await self.http_session.close()
            # See <https://aiohttp.readthedocs.io/en/stable/client_advanced.html#graceful-shutdown>
            await asyncio.sleep(0.250)

    async def reconnect(self):
        for chan in self.channels.values():
            chan.unsubscribed = True
        self.channels.clear()

        await super().reconnect()

    async def ping(self):
        response = await self.send_public_request({'event': 'ping'})
        assert response['event'] == 'pong'

    async def subscribe(self,
                        name: str,
                        pair: str,
                        depth: Optional[int] = None,
                        interval: Optional[int] = None) -> Channel:
        if depth is not None and name != 'book':
            raise ValueError("Depth applies only to the order book sub")
        if interval is not None and name != 'ohlc':
            raise ValueError("Interval applies only to the OHLC sub")

        token = None
        if name in ('ownTrades', 'openOrders'):
            token = await self.token_factory.get_token()
        sub = Subscription(name=name, depth=depth,
                           interval=interval, token=token)

        response = await self.send_public_request(data={
            'event': 'subscribe',
            'pair': [pair],
            'subscription': sub.as_dict()
        })
        assert response['event'] == 'subscriptionStatus'
        assert response['status'] == 'subscribed'

        channel: Channel
        if response['channelName'] in ('ownTrades', 'openOrders'):
            channel = PrivateChannel(api=self,
                                     name=response['channelName'],
                                     subscription=sub)
        else:
            channel = PublicChannel(api=self,
                                    id=response['channelID'],
                                    name=response['channelName'],
                                    pair=pair,
                                    subscription=sub)
        self.channels[channel_key(channel)] = channel
        return channel

    async def add_order(self):
        return NotImplemented

    async def cancel_order(self):
        return NotImplemented

    def _process_channel_message(self, msg):
        for channel in self.channels.values():
            if channel._try_enqueue(msg):
                logger.debug("Received message for channel %r: %r",
                             channel, msg)
                return
        raise KrakenWSError(
            "Received message from an unknown channel: {msg!r}")

    async def _process_published(self):
        msg = await self.receive_published()

        if isinstance(msg, list):
            self._process_channel_message(msg)
        elif msg['event'] == 'heartbeat':
            self.handle_heartbeat(msg)
        elif msg['event'] == 'systemStatus':
            self.handle_system_status(msg)
        elif msg['event'] == 'subscriptionStatus':
            raise KrakenWSError(
                f"Unsolicited subscription status message: {msg!r}")
        else:
            raise KrakenWSError(f"Unknown message type: {msg}")

    def handle_system_status(self, msg):
        if msg['status'] == 'online':
            logger.info("Kraken WS service is online. Version %s",
                        msg['version'])
        else:
            raise KrakenWSError(f"Kraken WS service is not online: {msg!r}")

        if msg['version'] != '0.2.0':
            logger.warning("Kraken WS service version is not 0.2.0")

    def handle_heartbeat(self, msg):
        logger.debug("Received heartbeat")


def channel_key(channel: Channel) -> Hashable:
    return (channel.id, channel.name, channel.pair)
